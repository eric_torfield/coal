# COAL

Collaboratively Organised Abuse List

This is a global abuse list that apps can make use of to ban/restrict or highlight users that have been identified
as abusers of the Blurt reward pool, with infringements such as farming rewards, copy-pasting repetitive content, fake accounts, plagiarism, et cetera.

Please refer to coal.json for the curated abuse list. Please submit merge requests for additions or appeals.

## Key
+ CFARM - Comment Farming: posting multiple low-effort (possibly automated) comments and self-voting (or voting with alt accounts) for the purpose of extracting rewards without adding value.
+ CPC - Copy-Pasted-Content: Copy/pasted content without citing source or adding significant own content.
+ FAKE - Fake account/indentity theft
+ FARM - Farming Rewards: making multiple low-effort (possibly automated) posts and self-voting (or voting with alt accounts) for the purpose of extracting rewards without adding value.
+ OTHER - Uncategorised, leave explanation in the note
+ PLG - Plagiarism


Naughty children get lumps of COAL in their stockings for Xmas! :)
